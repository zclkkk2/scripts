mkdir exthm
cd exthm
repo init -u https://github.com/exthmui/android.git -b exthm-10 --depth=1
repo sync -j8 --fail-fast --force-sync
source build/envsetup.sh
git clone https://gitlab.com/zclkkk/device_xiaomi_sdm845-common -b main device/xiaomi/sdm845-common
git clone https://gitlab.com/zclkkk/device_xiaomi_polaris -b main device/xiaomi/polaris
git clone https://gitlab.com/zclkkk/kernel_xiaomi_sdm845 -b build --depth=1 kernel/xiaomi/polaris
git clone https://gitlab.com/zclkkk/vendor_xiaomi_polaris -b main --depth=1 vendor/xiaomi/polaris
git clone https://gitlab.com/zclkkk/vendor_xiaomi_sdm845-common -b main --depth=1 vendor/xiaomi/sdm845-common
git clone https://gitlab.com/zclkkk/vendor_xiaomi-firmware.git -b main --depth=1 vendor/xiaomi-firmware
git clone https://github.com/LineageOS/android_hardware_xiaomi -b lineage-17.1 --depth=1 hardware/xiaomi
git clone https://github.com/kdrag0n/proton-clang prebuilts/clang/host/linux-x86/clang-proton --depth=1
lunch exthm_polaris-user
mka bacon -j8
